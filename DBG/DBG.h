/*
 * DBG.h
 *
 *  Created on: 08/08/2018
 *      Author: Ingenieria6
 */

#ifndef DBG_H_
#define DBG_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stddef.h>
#include <stdarg.h>
#include "main.h"
#include "usart.h"

#define DEBUG_ENABLE 			1

#define AVL_DBG_UART_HANDLE		huart2
#define AVL_DBG_UART_INSTANCE	USART2



#define DBG_UART_PORT	huart2

#define BOOL_PARAM(param)	(param ? '1' : '0')

void DBG_init(void);
void DBG_clear_screen(void);
void DBG_println(const char *fmt, ...);

void DBG_log_info(const char *fmt, ...);
void DBG_log_error(const char *fmt, ...);

void DBG_hexdump(uint8_t *buff, size_t len, size_t base);

void DBG_led_set(GPIO_TypeDef *pin_port, uint32_t pin);
void DBG_led_clear(GPIO_TypeDef *pin_port, uint32_t pin);
void DBG_led_toggle(GPIO_TypeDef *pin_port, uint32_t pin);

#ifdef __cplusplus
}
#endif

#endif /* DBG_H_ */
