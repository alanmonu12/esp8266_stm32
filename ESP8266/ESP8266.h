/*
 * ESP8266.h
 *
 *  Created on: 13/04/2019
 *      Author: Toshiba
 */

#ifndef ESP8266_H_
#define ESP8266_H_

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include "usart.h"
#include "dma.h"
#include "MQTTClient.h"

extern 	DMA_HandleTypeDef 			hdma_usart3_rx;

#define ESP8266_UART_HANDLE			huart3
#define ESP8266_UART_INSTANCE		USART3
#define ESP8266_UART_DMA			hdma_usart3_rx

#define ESP8266_END_OF_CMD_CHARACTER	'\r'
#define ESP8266_END_OF_CMD_CHARACTER_2 '\n'

#define ESP8266_DBG_ENABLED			1
#define ESP8266_REPLY_BUFFER_SIZE	1024
#define ESP8266_UART_WRITE_TIMEOUT		250

typedef enum
{
	STATION_MODE = 1,
	SOFTAP_MODE,
	SOFTAP_AND_STATION_MODE
}wifiMode_TypeDef;

typedef enum
{
	CONNECTED_TO_AN_AP_IP = 2,
	TCP_UDP_TRANSMISSION_CREATED = 3,
	TCP_UDP_TRANSMISSION_DISCONNECTED = 4,
	NOT_CONNECTED_TO_AP = 5
}ESP8266_CONNECTION_STATUS;

typedef enum
{
	NOT_SAVED_IN_FLASH,
	SAVED_IN_FLASH
}Config_State_TypeDef;

typedef struct ESP8266 ESP8266;

struct ESP8266
{
	wifiMode_TypeDef wifi_mode;
	ESP8266_CONNECTION_STATUS connection_status;
};
/*WIFI functions*/
void ESP8266_set_wifiMode(wifiMode_TypeDef wifiMode,Config_State_TypeDef state);

void ESP8266_connect_to_AP(char* ssid,char* password,Config_State_TypeDef state);
void ESP8266_disconnect_to_AP(void);

/*TCP/IP functions*/

void ESP8266_start_tcp_connection(char* ip, uint16_t port);

void ESP8266_send_data(char* payload);


void ESP8266_cmd_init(void);
void ESP8266_cmd_send(const char *fmt, ...);
void ESP8266_cmd_wait_reply(uint32_t timeout_ms);

void ESP8266_uart_handle_reply(void);
void ESP8266_uart_handle_idle(void);

ESP8266_CONNECTION_STATUS ESP8266_get_connection_status(void);

#endif /* ESP8266_H_ */
