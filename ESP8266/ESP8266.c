/*
 * ESP8266.c
 *
 *  Created on: 13/04/2019
 *      Author: Toshiba
 */

#include "ESP8266.h"
#include "DBG.h"
#include <string.h>


typedef struct ESP8266_Typedef ESP8266_Typedef;

struct ESP8266_Typedef
{
	wifiMode_TypeDef wifiMode;
};

volatile bool reply_recv = false;

char ESP8266_buffer_reply[ESP8266_REPLY_BUFFER_SIZE];

static uint8_t send_cmd(const char *cmd);
static void setup_data_reception(void);


void ESP8266_cmd_init(void)
{
	__HAL_DMA_DISABLE_IT(&ESP8266_UART_DMA, DMA_IT_HT);
	__HAL_DMA_ENABLE_IT(&ESP8266_UART_DMA, DMA_IT_TC);
	__HAL_UART_FLUSH_DRREGISTER(&ESP8266_UART_HANDLE);

	memset(ESP8266_buffer_reply,'\0',ESP8266_REPLY_BUFFER_SIZE);
}

void ESP8266_cmd_send(const char *fmt, ...)
{
	int format_result = -1;

	char cmd[1000];
	memset(cmd,'\0',sizeof(cmd));
	//clear_array(cmd, sizeof(cmd));

	// Clear receiver buffer
	memset(ESP8266_buffer_reply,'\0',ESP8266_REPLY_BUFFER_SIZE);

	va_list args;
	va_start(args, fmt);

	// Generar el string que se va a mandar por UART hacia el EG91
	format_result = vsnprintf(cmd, sizeof(cmd), fmt, args);

	va_end(args);

	if (0 > format_result) {
		// cmd no es lo suficientemente grande para almacenar el comando y
		// todos los par�metros, retornamos antes.
		return;
	} else {
		// cmd contiene el comando y los par�metros, a�adimos el car�cter que
		// indica el fin de comando al string y lo enviamos por serial hacia el EG91
		cmd[format_result] = ESP8266_END_OF_CMD_CHARACTER;
		cmd[format_result + 1] = ESP8266_END_OF_CMD_CHARACTER_2;

#if (ESP8266_DBG_ENABLED == 1)
		DBG_println("ESP8266 cmd > %s", cmd);
#endif

		send_cmd(cmd);
	}
}


/**
 *
 */
void ESP8266_cmd_wait_reply(uint32_t timeout_ms)
{
	bool timeout = false;
	uint32_t tick_start = 0;

	reply_recv = false;

	setup_data_reception();

	tick_start = HAL_GetTick();

	while (!reply_recv) {
		if ((HAL_GetTick() - tick_start) > timeout_ms) {
			timeout = true;
			break;
		}
	}

	reply_recv = false;

#if (ESP8266_DBG_ENABLED == 1)
	if (timeout) {
		DBG_println("ESP8266 rsp < TIMEOUT");
	} else {
		DBG_println("ESP8266 rsp < %s", ESP8266_buffer_reply);
	}
#endif
}

void ESP8266_uart_handle_reply(void)
{

			reply_recv = true;

}

void ESP8266_uart_handle_idle(void)
{
	if (RESET != __HAL_UART_GET_FLAG(&ESP8266_UART_HANDLE, UART_FLAG_IDLE)) {
		if (RESET != __HAL_UART_GET_IT_SOURCE(&ESP8266_UART_HANDLE, UART_IT_IDLE)) {

			__HAL_UART_CLEAR_IDLEFLAG(&ESP8266_UART_HANDLE);
			__HAL_UART_DISABLE_IT(&ESP8266_UART_HANDLE, UART_IT_IDLE);

			// Detenemos la transferencia de datos por el DMA y forzamos la
			// ejecuci�n del callback de recepci�n de datos.
			HAL_UART_DMAStop(&ESP8266_UART_HANDLE);
			if (NULL != ESP8266_UART_DMA.XferCpltCallback) {
				ESP8266_UART_DMA.XferCpltCallback(&ESP8266_UART_DMA);
			}
		}
	}
}

void ESP8266_set_wifiMode(wifiMode_TypeDef wifiMode,Config_State_TypeDef state)
{
	if(state == NOT_SAVED_IN_FLASH)
	{
		ESP8266_cmd_send("AT+CWMODE_CUR=%d",wifiMode);
	}
	else
	{
		ESP8266_cmd_send("AT+CWMODE_DEF=%d",wifiMode);
	}

	  ESP8266_cmd_wait_reply(1000);
}

//TODO:|Verificar si la conexion fue exitosa
void ESP8266_connect_to_AP(char* ssid,char* password,Config_State_TypeDef state)
{
	if(NOT_CONNECTED_TO_AP == ESP8266_get_connection_status())
	{
		if(state == NOT_SAVED_IN_FLASH)
		{
			ESP8266_cmd_send("AT+CWJAP_CUR=\"%s\",\"%s\"",ssid,password);
		}
		else
		{
			ESP8266_cmd_send("AT+CWJAP_DEF=\"%s\",\"%s\"",ssid,password);
		}
	}
	else
	{
		ESP8266_disconnect_to_AP();
		if(state == NOT_SAVED_IN_FLASH)
		{
			ESP8266_cmd_send("AT+CWJAP_CUR=\"%s\",\"%s\"",ssid,password);
		}
		else
		{
			ESP8266_cmd_send("AT+CWJAP_DEF=\"%s\",\"%s\"",ssid,password);
		}
	}
	  ESP8266_cmd_wait_reply(5000);
	  ESP8266_cmd_wait_reply(30000);
}

void ESP8266_disconnect_to_AP(void)
{
	ESP8266_cmd_send("AT+CWQAP");
	ESP8266_cmd_wait_reply(5000);
}

//TODO: Verificar si el socket se abrio de manera correcta
//TODO: Verificar el estado de la conexion antes de abrir un nuevo socket
void ESP8266_start_tcp_connection(char* ip, uint16_t port)
{
	ESP8266_cmd_send("AT+CIPSTART=\"TCP\",\"%s\",%u",ip,port);
	ESP8266_cmd_wait_reply(5000);
}

//TODO: Verificar si los datos se enviaron de manera correcta
//TODO: Verificcar el estado de la conexion antes de enviar los datos
void ESP8266_send_data(char* payload)
{
	ESP8266_cmd_send("AT+CIPSEND=%d",strlen(payload)+2);
	ESP8266_cmd_wait_reply(1000);
	HAL_Delay(5000);
	ESP8266_cmd_send("%s",payload);
	ESP8266_cmd_wait_reply(1000);
}

//AT+CIPSTATUS
ESP8266_CONNECTION_STATUS ESP8266_get_connection_status(void)
{
	uint8_t status = NOT_CONNECTED_TO_AP;
	ESP8266_cmd_send("AT+CIPSTATUS");
	ESP8266_cmd_wait_reply(1000);

	if(NULL != strstr(ESP8266_buffer_reply,"2"))
	{
		status = CONNECTED_TO_AN_AP_IP;
		return status;
	}
	if(NULL != strstr(ESP8266_buffer_reply,"3"))
	{
		status = TCP_UDP_TRANSMISSION_CREATED;
		return status;
	}
	if(NULL != strstr(ESP8266_buffer_reply,"4"))
	{
		status = TCP_UDP_TRANSMISSION_DISCONNECTED;
		return status;
	}
	if(NULL != strstr(ESP8266_buffer_reply,"5"))
	{
		status = NOT_CONNECTED_TO_AP;
		return status;
	}

	return status;
}


/*Funciones privadas*/
static uint8_t send_cmd(const char *cmd)
{
	return HAL_UART_Transmit(&ESP8266_UART_HANDLE, (uint8_t *) cmd, strlen(cmd),
							 ESP8266_UART_WRITE_TIMEOUT);
}

static void setup_data_reception(void)
{
	// Solo activamos la interrupci�n por IDLE cuando no son mensajes URC.
	memset(ESP8266_buffer_reply,'\0',sizeof(ESP8266_buffer_reply));
	__HAL_UART_CLEAR_IDLEFLAG(&ESP8266_UART_HANDLE);
	__HAL_UART_ENABLE_IT(&ESP8266_UART_HANDLE, UART_IT_IDLE);
	__HAL_UART_FLUSH_DRREGISTER(&ESP8266_UART_HANDLE);
	HAL_UART_Receive_DMA(&ESP8266_UART_HANDLE, (uint8_t *) ESP8266_buffer_reply, sizeof(ESP8266_buffer_reply));
}
