/*
 * MQTT_STM32.h
 *
 *  Created on: 23/05/2019
 *      Author: equipo
 */

#ifndef MQTT_STM32_H_
#define MQTT_STM32_H_

#define pdTRUE   ( 1 )
#define pdFALSE  ( 0 )

#include "stm32f3xx.h"
#include "ESP8266.h"
#include "MQTTClient.h"
#include "string.h"

/*Funciones para el manejo de tiempo*/
void TimerCountdownMS(Timer* timer, unsigned int timeout_ms);

void TimerCountdown(Timer* timer, unsigned int timeout);

int TimerLeftMS(Timer* timer);

char TimerIsExpired(Timer* timer);

void TimerInit(Timer* timer);

/*Funciones para el manejo de las conexiones con el servidor*/
int MQTT_STM32_read(Network* n, unsigned char* buffer, int len, int timeout_ms);

int MQTT_STM32_write(Network* n, unsigned char* buffer, int len, int timeout_ms);

void MQTT_STM32_disconnect(Network* n);

/*Funciones para la network*/
void NetworkInit(Network* n);

int NetworkConnect(Network* n, char* addr, int port);


#endif /* MQTT_STM32_H_ */
