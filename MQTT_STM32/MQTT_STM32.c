/*
 * MQTT_STM32.c
 *
 *  Created on: 23/05/2019
 *      Author: equipo
 */

#include "MQTT_STM32.h"


extern char ESP8266_buffer_reply[ESP8266_REPLY_BUFFER_SIZE];

void TimerCountdownMS(Timer* timer, unsigned int timeout_ms)
{
	timer->xTicksToWait = timeout_ms;
	timer->xTimeOut = HAL_GetTick();
}


void TimerCountdown(Timer* timer, unsigned int timeout)
{
	TimerCountdownMS(timer, timeout * 1000);
}


int TimerLeftMS(Timer* timer)
{
	int tick = HAL_GetTick();

	timer->xTicksToWait = timer->xTicksToWait - (tick - timer->xTimeOut);
	/* updates xTicksToWait to the number left */
	return (timer->xTicksToWait < 0) ? 0 : timer->xTicksToWait;
}


char TimerIsExpired(Timer* timer)
{

	int tick = HAL_GetTick();

	timer->xTicksToWait = timer->xTicksToWait - (tick - timer->xTimeOut);
	/* updates xTicksToWait to the number left */
	return (timer->xTicksToWait < 0) ? pdFALSE : pdTRUE;
}


void TimerInit(Timer* timer)
{
	timer->xTicksToWait = 0;
	timer->xTimeOut = 0;
}


int MQTT_STM32_read(Network* n, unsigned char* buffer, int len, int timeout_ms)
{
	int recvLen = 0;

	//xTimeOut = HAL_GetTick();/* Record the time at which this function was entered. */

	ESP8266_cmd_wait_reply(timeout_ms);

	recvLen = strlen((const char*)ESP8266_buffer_reply);

	return recvLen;
}


int MQTT_STM32_write(Network* n, unsigned char* buffer, int len, int timeout_ms)
{

	int sentLen = 0;

	ESP8266_send_data((char*)buffer);

	sentLen = strlen((const char*) ESP8266_buffer_reply);
	return sentLen;
}


void MQTT_STM32_disconnect(Network* n)
{

}


void NetworkInit(Network* n)
{
	n->sckt.a = 0;
	n->mqttread = MQTT_STM32_read;
	n->mqttwrite = MQTT_STM32_write;
	n->disconnect = MQTT_STM32_disconnect;
}


int NetworkConnect(Network* n, char* addr, int port)
{
	//TODO: Detectar cuando no se puede conectar al servidor
	int retVal = 1;

	ESP8266_set_wifiMode(STATION_MODE,SAVED_IN_FLASH);

	ESP8266_connect_to_AP("INFINITUM1072_2.4","dVzH3CCD6T",NOT_SAVED_IN_FLASH);

	HAL_Delay(5000);

	ESP8266_start_tcp_connection(addr,port);

	return retVal;
}
